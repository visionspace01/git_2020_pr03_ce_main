/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon May 4 02:26:19 2020
**      by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGraphicsView *gView1;
    QPushButton *btnTest;
    QPushButton *btnCamConfig;
    QPushButton *btnEmail;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QMenuBar *menuBar;
    QMenu *menuEyeOnHome;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(701, 478);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gView1 = new QGraphicsView(centralWidget);
        gView1->setObjectName(QString::fromUtf8("gView1"));
        gView1->setGeometry(QRect(10, 10, 531, 421));
        btnTest = new QPushButton(centralWidget);
        btnTest->setObjectName(QString::fromUtf8("btnTest"));
        btnTest->setGeometry(QRect(570, 10, 83, 25));
        btnCamConfig = new QPushButton(centralWidget);
        btnCamConfig->setObjectName(QString::fromUtf8("btnCamConfig"));
        btnCamConfig->setGeometry(QRect(570, 70, 83, 25));
        btnEmail = new QPushButton(centralWidget);
        btnEmail->setObjectName(QString::fromUtf8("btnEmail"));
        btnEmail->setGeometry(QRect(570, 350, 83, 25));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(570, 140, 83, 25));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(570, 210, 83, 25));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 701, 22));
        menuEyeOnHome = new QMenu(menuBar);
        menuEyeOnHome->setObjectName(QString::fromUtf8("menuEyeOnHome"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuEyeOnHome->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "EyeOnHome", 0, QApplication::UnicodeUTF8));
        btnTest->setText(QApplication::translate("MainWindow", "TEST", 0, QApplication::UnicodeUTF8));
        btnCamConfig->setText(QApplication::translate("MainWindow", "CONFIGURE", 0, QApplication::UnicodeUTF8));
        btnEmail->setText(QApplication::translate("MainWindow", "Send Photo", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "ShowLive", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("MainWindow", "TakePhoto", 0, QApplication::UnicodeUTF8));
        menuEyeOnHome->setTitle(QApplication::translate("MainWindow", "EyeOnHome", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
