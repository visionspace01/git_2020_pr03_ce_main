#-------------------------------------------------
#
# Project created by QtCreator 2020-05-01T14:26:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EyeOnHome
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

#-----Opencv lib and include path----
INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core  -lopencv_imgcodecs -lopencv_highgui

#----------------------------



unix:!macx:!symbian: LIBS += -L$$PWD/src/CameraAcquisition/ -lCameraAcquisition

INCLUDEPATH += $$PWD/src/CameraAcquisition
DEPENDPATH += $$PWD/src/CameraAcquisition
