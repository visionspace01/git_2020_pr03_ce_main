#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_btnTest_clicked();

    void on_btnCamConfig_clicked();

    void on_btnEmail_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
