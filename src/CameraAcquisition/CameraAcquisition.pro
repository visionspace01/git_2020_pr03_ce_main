#-------------------------------------------------
#
# Project created by QtCreator 2020-05-03T04:05:28
#
#-------------------------------------------------

QT       -= gui

TARGET = CameraAcquisition
TEMPLATE = lib

DEFINES += CAMERAACQUISITION_LIBRARY

SOURCES += cameraacquisition.cpp

#--------Opencv LIB and Include path-------

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui

#---------------------------

HEADERS += cameraacquisition.h\
        CameraAcquisition_global.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE0357FFF
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = CameraAcquisition.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
