#ifndef CAMERAACQUISITION_H
#define CAMERAACQUISITION_H

#include "CameraAcquisition_global.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>




class CAMERAACQUISITIONSHARED_EXPORT CameraAcquisition
{
public:
    CameraAcquisition(int id);
    CameraAcquisition();

    //void test();
    bool Init(int ID);
    bool Capture(int ID);
    bool Live();
private:
    int cameraId;
};

#endif // CAMERAACQUISITION_H
